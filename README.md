
<img src="./fil/i.jpg" align="center"></img>

---

<h1 align="center"> 🕋 libdump-1 </h1>
<h2 align="center">Personal Research Libray </h2>

----
1. [What ?](#what-)
2. [Tree](#tree)
3. [Resources](#resources)

----

# What ? 

> This will contain my main sources books all translated into English 

# Tree 

Tree structure 

Dir | Description 
|:--:|:--:|
[`cmds`](./cmds/) | Custom commands 
[`I1`](./I1/) | Main Dump 

# Resources 

N | Description
|:--:|:--:|
`gfx` | [`Islam Wallpaper`](https://www.wallpaperflare.com/search?wallpaper=muslim)


